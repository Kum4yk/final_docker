import asyncio
import os
from pathlib import Path
import pandas as pd
from aiohttp import web
from routes import setup_routes
from models.bert_summarizer import create_bert_summarizer
from models.markovify import create_markov_model
from models.bert2bert import create_ber2bert


async def init_app(markov, bert_sum, bert2bert) -> web.Application:
    """Initialize the application server."""
    app = web.Application()

    app["markov"] = markov
    app['bert_sum'] = bert_sum
    app["bert2bert"] = bert2bert

    setup_routes(app)
    return app


if __name__ == "__main__":
    PATH_TO_NEWS = os.path.join(
        Path(__file__).parent, "data", "clean_ria_1000000.csv"
        )

    print(PATH_TO_NEWS)
    data = pd.read_csv(PATH_TO_NEWS)
    bert_sum_model = create_bert_summarizer()
    markov_model = create_markov_model(data.title)
    bert2bert_model = create_ber2bert()

    loop = asyncio.get_event_loop()
    application = loop.run_until_complete(
        init_app(
            markov_model,
            bert_sum_model,
            bert2bert_model
        )
    )

    print("\n\n\nGo here - http://localhost:8080/")

    web.run_app(
        application
    )
