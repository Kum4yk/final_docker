from aiohttp import web
import aiohttp_cors
from handlers.handlers import markov_handler, welcome_handler, seq2seq_handler, summarizer_handler
from typing import NoReturn


def setup_routes(app: web.Application) -> NoReturn:
    """Setup endpoints for application"""
    cors = aiohttp_cors.setup(
        app,
        defaults={
            "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*"
                )
            }
    )

    app.router.add_route("GET", "/", welcome_handler)
    app.router.add_route("GET", "/markov", markov_handler)
    app.router.add_route("GET", "/markov/{counts}", markov_handler)
    app.router.add_route("GET", "/summarizer/{text}", summarizer_handler)
    app.router.add_route("GET", "/bert2bert/{text}", seq2seq_handler)

    for route in list(app.router.routes()):
        cors.add(route)
