import markovify
import pandas as pd


def create_markov_model(titles: pd.Series):
    markov_model = markovify.NewlineText(titles, state_size=2)
    return markov_model
