FROM python:3.8

RUN mkdir -p /final_docker
WORKDIR /final_docker
COPY . /final_docker

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
RUN ln -s /root/.poetry/bin/poetry /usr/bin/poetry
ENV PYTHONPATH "${PYTHONPATH}:/final_docker/src"
RUN poetry config virtualenvs.create false
RUN poetry install

EXPOSE 8080

CMD ["python", "/final_docker/src/server.py"]
